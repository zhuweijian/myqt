#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif

#include "MainWindow.h"
#include <qlayout.h>
#include <qdebug.h>
#include <string>
#include <iostream>
using namespace std;

//using std::string;
//using std::shared_ptr;
//using std::make_shared;
//using std::count;

MainWindow::MainWindow(){
    loginButton = new QPushButton("登陆");
    //设置关联
    connect(loginButton, SIGNAL(clicked()), this, SLOT(click_login()));
    //设置布局
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(loginButton);
    //设置窗口
    QWidget *mainWidget = new QWidget();
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);
    setWindowTitle("测试交易API");
    show();
}


void MainWindow::click_login(){
    cout << "###### click_login() ####### " << endl;
}
