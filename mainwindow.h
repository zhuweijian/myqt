#pragma once

#include <qmainwindow.h>
#include <qpushbutton.h>
#include <memory>

class MainWindow :public QMainWindow{
    Q_OBJECT
public:
    MainWindow();
private:
    QPushButton *loginButton;
private slots:
    void click_login();
};
